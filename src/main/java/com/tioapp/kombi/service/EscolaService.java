package com.tioapp.kombi.service;

import java.util.List;

import com.tioapp.kombi.model.Escola;

public interface EscolaService {
	
	public List<Escola> escolas(String nome, String regiao);
	public Escola escola(String id);
	public boolean existeEscola(String id);

}