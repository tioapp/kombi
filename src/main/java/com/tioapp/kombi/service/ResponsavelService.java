package com.tioapp.kombi.service;

import com.tioapp.kombi.model.Responsavel;

public interface ResponsavelService {
	
	public Responsavel responsavel(String id);
	public Responsavel salvarResponsavelLogin(String id, String email, String nome);
	public void salvarResponsavel(Responsavel responsavel);
	public void atualizarResponsavel(Responsavel responsavel);
}