package com.tioapp.kombi.service;

import com.tioapp.kombi.model.Endereco;

public interface EnderecoService {
	
	public Endereco enderecoByEscola(String escolaId);
	public Endereco enderecoByResponsavel(String responsavelId);
	public void salvarEnderecoByEscola(String escolaId, Endereco endereco);
	public void salvarEnderecoByResponsavel(String escolaId, Endereco endereco);
}