package com.tioapp.kombi.service;

import java.util.List;

import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.model.Motorista;

public interface MotoristaService {
	
	public Motorista motorista(String id);
	public void salvarMotorista(Motorista motorista);
	public Motorista salvarMotoristaLogin(String id, String email, String nome);
	public void atualizarMotorista(Motorista motorista);
	public void salvarEscola(String id, Escola escola);
	public List<Motorista> motoristas (String escolaId);
}