package com.tioapp.kombi.service;

import com.tioapp.kombi.model.Login;

public interface LoginService {
	
	public Login login(Login login);
	public Login autenticacao(Login login);

}