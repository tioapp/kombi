package com.tioapp.kombi.service;

import java.util.List;

import com.tioapp.kombi.model.Telefone;

public interface TelefoneService {
	
	public List<Telefone> telefonesByEscola(String escolaId);
	public List<Telefone> telefonesByResponsavel(String responsavelId);
	public void salvarTelefonesByEscola(String escolaId, List<Telefone> telefones);

}