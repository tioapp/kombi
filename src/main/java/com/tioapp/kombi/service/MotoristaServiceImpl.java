package com.tioapp.kombi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.MotoristaEntity;
import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.exception.UnprocessableEntityException;
import com.tioapp.kombi.mapper.MotoristaMapper;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.model.Motorista;
import com.tioapp.kombi.repository.EscolaRepository;
import com.tioapp.kombi.repository.MotoristaRepository;

@Service
public class MotoristaServiceImpl implements MotoristaService {
	
	@Autowired
	private MotoristaRepository motoristaRepository;
	
	@Autowired
	private MotoristaMapper motoristaMapper;
	
	@Autowired
	private EscolaService escolaService;
	
	@Autowired
	private EscolaRepository escolaRepository;

	@Override
	public Motorista motorista(String id) {
		try {
			MotoristaEntity motoristaEntity = motoristaRepository.findById(id)
					.orElseThrow(() -> new NotFoundException());
			
			return motoristaMapper.buildMotorista(motoristaEntity);
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarMotorista(Motorista motorista) {
		try {
			if(StringUtils.isBlank(motorista.getId()) || !motoristaRepository.existsById(motorista.getId())) {
				motoristaRepository.save(motoristaMapper.buildMotoristaEntity(motorista));
			
			} else {
				List<FieldError> validationErrors = new ArrayList<FieldError>();
				validationErrors.add(FieldError.builder()
						.name("id")
						.error("Responsavel já existe")
						.build());
				throw new UnprocessableEntityException(validationErrors);
			}
			
		} catch (DuplicateKeyException e) {
			throw new InternalServerException(e.getCause().getMessage());
			
		} catch (BadRequestException e) {
			throw e;
			
		} catch (UnprocessableEntityException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
	
	@Override
	public Motorista salvarMotoristaLogin(String id, String email, String nome) {
		MotoristaEntity motoristaEntity = motoristaRepository.save(
				motoristaMapper.buildMotoristaEntity(
						id, 
						email, 
						nome));
		
		return motoristaMapper.buildMotorista(motoristaEntity); 
	}
	
	@Override
	public void atualizarMotorista(Motorista motorista) {
		try {
			MotoristaEntity motoristaEntity = motoristaRepository.findById(motorista.getId())
					.orElseThrow(() -> new NotFoundException());
			
			motoristaRepository.save(
					motoristaMapper.buildMotoristaEntity(
							motorista,
							motoristaEntity.getEndereco(), 
							motoristaEntity.getTelefones()));
		
		} catch (NotFoundException e) {
			throw e;
			
		} catch (DuplicateKeyException e) {
			throw new InternalServerException(e.getCause().getMessage());
			
		} catch (BadRequestException e) {
			throw e;
			
		} catch (UnprocessableEntityException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarEscola(String id, Escola escola) {
		try {
			MotoristaEntity motoristaEntity = motoristaRepository.findById(id)
					.orElseThrow(() -> new NotFoundException());
			
			if(escolaService.existeEscola(escola.getId())) {
				motoristaRepository.save(motoristaMapper.buildMotoristaEntity(motoristaEntity,escola));
			
			} else {
				throw new NotFoundException();
			}
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public List<Motorista> motoristas(String escolaId) {
		//EscolaEntity escolaEntity = escolaRepository.findById(escolaId).orElse(null);
		List<MotoristaEntity> motoristasEntity = motoristaRepository.findByEscolasId(escolaId);
		return motoristaMapper.buildMotoristas(motoristasEntity);
	}

}