package com.tioapp.kombi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.ResponsavelEntity;
import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.exception.UnprocessableEntityException;
import com.tioapp.kombi.mapper.ResponsavelMapper;
import com.tioapp.kombi.model.Responsavel;
import com.tioapp.kombi.repository.ResponsavelRepository;

@Service
public class ResponsavelServiceImpl implements ResponsavelService {
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
	private ResponsavelMapper responsavelMapper;

	@Override
	public Responsavel responsavel(String id) {
		try {
			ResponsavelEntity responsavelEntity = responsavelRepository.findById(id)
					.orElseThrow(() -> new NotFoundException());
			
			return responsavelMapper.buildResponsavel(responsavelEntity);
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarResponsavel(Responsavel responsavel) {
		try {
			if(StringUtils.isBlank(responsavel.getId()) || !responsavelRepository.existsById(responsavel.getId())) {
				responsavelRepository.save(responsavelMapper.buildResponsavelEntity(responsavel));
			
			} else {
				List<FieldError> validationErrors = new ArrayList<FieldError>();
				validationErrors.add(FieldError.builder()
						.name("id")
						.error("Responsavel já existe")
						.build());
				throw new UnprocessableEntityException(validationErrors);
			}
			
		} catch (DuplicateKeyException e) {
			throw new InternalServerException(e.getCause().getMessage());
			
		} catch (BadRequestException e) {
			throw e;
			
		} catch (UnprocessableEntityException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
	
	@Override
	public Responsavel salvarResponsavelLogin(String id, String email, String nome) {
		ResponsavelEntity responsavelEntity = responsavelRepository.save(
				responsavelMapper.buildResponsavelEntity(
						id, 
						email, 
						nome));
		
		return responsavelMapper.buildResponsavel(responsavelEntity); 
	}
	
	@Override
	public void atualizarResponsavel(Responsavel responsavel) {
		try {
			ResponsavelEntity responsavelEntity = responsavelRepository.findById(responsavel.getId())
					.orElseThrow(() -> new NotFoundException());
			
			responsavelRepository.save(
					responsavelMapper.buildResponsavelEntity(
							responsavel,
							responsavelEntity.getEndereco(),
							responsavelEntity.getTelefones()));
		
		} catch (NotFoundException e) {
			throw e;
			
		} catch (DuplicateKeyException e) {
			throw new InternalServerException(e.getCause().getMessage());
			
		} catch (BadRequestException e) {
			throw e;
			
		} catch (UnprocessableEntityException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

}