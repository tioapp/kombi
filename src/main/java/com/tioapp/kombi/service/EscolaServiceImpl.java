package com.tioapp.kombi.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.mapper.EscolaMapper;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.repository.EscolaRepository;

@Service
public class EscolaServiceImpl implements EscolaService {
	
	@Autowired
	private EscolaRepository escolaRepository;
	
	@Autowired
	private EscolaMapper escolaMapper;

	@Override
	public List<Escola> escolas(String nome, String regiao) {
		List<EscolaEntity> escolaEntities = null;
		
		try {
			if (StringUtils.isNotBlank(nome)) {
				escolaEntities = escolaRepository.findByNomeLike(StringUtils.upperCase(nome));
			
			}else if (StringUtils.isNotBlank(regiao)) {
					escolaEntities = escolaRepository.findByEnderecoBairroLike(StringUtils.upperCase(regiao));
					
			} else {
				escolaEntities = escolaRepository.findAll();
			}
			
			if (!escolaEntities.isEmpty()) {
				return escolaMapper.buildEscolas(escolaEntities);
			
			} else {
				throw new NotFoundException();
			}
		
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
 	public Escola escola(String id) {
		try {
			EscolaEntity escolaEntity = escolaRepository.findById(id)
					.orElseThrow(() -> new NotFoundException());
			
			return escolaMapper.buildEscola(escolaEntity);
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public boolean existeEscola(String id) {
		return escolaRepository.existsById(id);
	}
}