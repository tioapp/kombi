package com.tioapp.kombi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.LoginEntity;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.exception.UnprocessableEntityException;
import com.tioapp.kombi.mapper.LoginMapper;
import com.tioapp.kombi.model.Login;
import com.tioapp.kombi.model.Motorista;
import com.tioapp.kombi.model.Responsavel;
import com.tioapp.kombi.model.TipoLoginEnum;
import com.tioapp.kombi.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private LoginMapper loginMapper;
	
	@Autowired
	private ResponsavelService responsavelService;
	
	@Autowired
	private MotoristaService motoristaService;

	@Override
	public Login login(Login login) {
		try {
			LoginEntity loginEntity = loginRepository.save(loginMapper.buildLoginEntity(login));
			Responsavel responsavel = null;
			Motorista motorista = null;
			
			if(StringUtils.equals(loginEntity.getTipo(), TipoLoginEnum.RESPONSAVEL.name())) {
				responsavel = responsavelService.salvarResponsavelLogin(
						loginEntity.getId(),
						loginEntity.getLogin(),
						loginEntity.getNome());
				
				return loginMapper.buildLogin(loginEntity, responsavel);
				
			} else 	if(StringUtils.equals(loginEntity.getTipo(), TipoLoginEnum.MOTORISTA.name())) {
				motorista = motoristaService.salvarMotoristaLogin(
						loginEntity.getId(),
						loginEntity.getLogin(),
						loginEntity.getNome());
				
				return loginMapper.buildLogin(loginEntity, motorista);
			}
			
			throw new InternalServerException();
			
		
		} catch (DuplicateKeyException e) {
			throw new InternalServerException(e.getCause().getMessage());
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public Login autenticacao(Login login) {
		Responsavel responsavel = null;
		Motorista motorista = null;
		
		try {
			List<LoginEntity> logins = loginRepository.findByLogin(login.getLogin());
			
			if (!logins.isEmpty()) {
				LoginEntity loginEntity = logins.stream()
						.filter(loginFilter -> StringUtils.equals(loginFilter.getSenha(), login.getSenha()))
						.filter(loginFilter -> StringUtils.equals(loginFilter.getTipo(), login.getTipo().name()))			           
						.findFirst()
						.orElse(null);
				
				if (loginEntity != null) {
					if(StringUtils.equals(loginEntity.getTipo(), TipoLoginEnum.RESPONSAVEL.name())) {
						responsavel = responsavelService.responsavel(loginEntity.getId());
						
						return loginMapper.buildLogin(loginEntity,responsavel);
					
					} else if(StringUtils.equals(loginEntity.getTipo(), TipoLoginEnum.MOTORISTA.name())) {
						motorista = motoristaService.motorista(loginEntity.getId());
						
						return loginMapper.buildLogin(loginEntity,motorista);
					}
				
				} else {
					List<FieldError> validationErrors = new ArrayList<FieldError>();
					validationErrors.add(FieldError.builder()
							.name("login/senha")
							.error("Seu login ou senha estão incorretos.")
							.build());
					throw new UnprocessableEntityException(validationErrors);
				}
				
			} else {
				throw new NotFoundException();
			}
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (UnprocessableEntityException e) {
			throw e;
		
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
		
		throw new InternalServerException();
	}
}