package com.tioapp.kombi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.ResponsavelEntity;
import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.mapper.EscolaMapper;
import com.tioapp.kombi.mapper.TelefoneMapper;
import com.tioapp.kombi.model.Telefone;
import com.tioapp.kombi.repository.EscolaRepository;
import com.tioapp.kombi.repository.ResponsavelRepository;

@Service
public class TelefoneServiceImpl implements TelefoneService {
	
	@Autowired
	private EscolaRepository escolaRepository;
	
	@Autowired
	private TelefoneMapper telefoneMapper;
	
	@Autowired
	private EscolaMapper escolaMapper;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;

	@Override
	public List<Telefone> telefonesByEscola(String escolaId) {
		try {
			EscolaEntity escolaEntity = escolaRepository.findById(escolaId)
					.orElseThrow(() -> new NotFoundException());
			
			return telefoneMapper.buildTelefones(escolaEntity);
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
	
	public List<Telefone> telefonesByResponsavel(String responsavelId) {
		try {
			ResponsavelEntity responsavelEntity = responsavelRepository.findById(responsavelId)
					.orElseThrow(() -> new NotFoundException());
			
			return telefoneMapper.buildTelefones(responsavelEntity);
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarTelefonesByEscola(String escolaId, List<Telefone> telefones) {
		try {
			if (!telefones.isEmpty()) {
				EscolaEntity escolaEntity = escolaRepository.findById(escolaId)
						.orElseThrow(() -> new NotFoundException());
				
				escolaRepository.save(escolaMapper.buildTelefonesEntity(escolaEntity, telefones));
			} else {
				List<FieldError> validationErrors = new ArrayList<FieldError>();
				validationErrors.add(FieldError.builder()
						.name("telefones")
						.error("Lista de Telefone invalida")
						.build());
				throw new BadRequestException(validationErrors);
			}

		} catch (BadRequestException e) {
			throw e;
		
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
}