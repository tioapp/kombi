package com.tioapp.kombi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.ResponsavelEntity;
import com.tioapp.kombi.exception.InternalServerException;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.mapper.EnderecoMapper;
import com.tioapp.kombi.mapper.EscolaMapper;
import com.tioapp.kombi.mapper.ResponsavelMapper;
import com.tioapp.kombi.model.Endereco;
import com.tioapp.kombi.repository.EscolaRepository;
import com.tioapp.kombi.repository.ResponsavelRepository;

@Service
public class EnderecoServiceImpl implements EnderecoService {

	@Autowired
	private EscolaRepository escolaRepository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
	private EnderecoMapper enderecoMapper;
	
	@Autowired
	private EscolaMapper escolaMapper;
	
	@Autowired
	private ResponsavelMapper responsavelMapper;
	
	@Override
	public Endereco enderecoByEscola(String escolaId) {
		try {
			EscolaEntity escolaEntity = escolaRepository.findById(escolaId)
					.orElseThrow(() -> new NotFoundException());
			
			return enderecoMapper.buildEndereco(escolaEntity);
			
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
	
	@Override
	public Endereco enderecoByResponsavel(String responsavelId) {
		try {
			 ResponsavelEntity responsavelEntity = responsavelRepository.findById(responsavelId)
					.orElseThrow(() -> new NotFoundException());
			
			return enderecoMapper.buildEndereco(null,responsavelEntity.getEndereco());
			
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarEnderecoByEscola(String escolaId, Endereco endereco) {
		try {
			EscolaEntity escolaEntity = escolaRepository.findById(escolaId)
					.orElseThrow(() -> new NotFoundException());
			
			escolaRepository.save(escolaMapper.buildEscolaEntity(escolaEntity, endereco));
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@Override
	public void salvarEnderecoByResponsavel(String escolaId, Endereco endereco) {
		try {
			ResponsavelEntity responsavelEntity = responsavelRepository.findById(escolaId)
					.orElseThrow(() -> new NotFoundException());
			
			responsavelRepository.save(responsavelMapper.buildResponsavelEntity(responsavelEntity, endereco));
			
		} catch (NotFoundException e) {
			throw e;
			
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}
}