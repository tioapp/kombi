package com.tioapp.kombi.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper=false)
@Valid
public class Telefone extends RepresentationModel<Telefone> {
	
	@NotNull
	private TipoTelefoneEnum tipo;
	@NotEmpty
	private String ddd;
	@NotEmpty
	private String numero;

}