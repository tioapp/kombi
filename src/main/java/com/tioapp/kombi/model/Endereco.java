package com.tioapp.kombi.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper=false)
@Valid
@JsonInclude(Include.NON_NULL)
public class Endereco extends RepresentationModel<Endereco> {
	
	@NotNull
	private String rua;
	@NotNull
	private String bairro;
	@NotNull
	private String numero;
	private String complemento;
	@NotNull
	private String cidade;
	@NotNull
	private String estado;

}