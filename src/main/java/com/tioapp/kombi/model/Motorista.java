package com.tioapp.kombi.model;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper=false)
@Valid
@JsonInclude(Include.NON_NULL)
public class Motorista extends RepresentationModel<Motorista> {
	
	private String id;
	@NotEmpty
	
	private String nome;
	
	@NotEmpty
	@CPF
	private String documentoCpf;
	private String documentoRg;
	private EstadoCivilEnum estadoCivil;
	
	@NotNull
	private LocalDate dataNascimento;
	
	@NotEmpty 
	@Email
	private String email;
	
	
	private List<Telefone> telefones;
	private Endereco endereco;
	
	private List<Escola> escolas;
}