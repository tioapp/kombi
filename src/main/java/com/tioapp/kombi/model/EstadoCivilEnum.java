package com.tioapp.kombi.model;

public enum EstadoCivilEnum {
	CASADO,
	SOLTEIRO,
	DIVORCIADO,
	VIUVO,
	SEPARADO;

}