package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Endereco;
import com.tioapp.kombi.service.EnderecoService;

@RestController
@RequestMapping(value = "/kombi", produces = MediaType.APPLICATION_JSON_VALUE)
public class EnderecoController {
	
	@Autowired
	private EnderecoService enderecoService;
	
    @GetMapping("/escolas/{escolaId}/endereco")
    @ResponseBody
    public ResponseEntity<Endereco> enderecoEscola (
    		@PathVariable("escolaId") String id) {
    	
        return new ResponseEntity<Endereco>(enderecoService.enderecoByEscola(id), HttpStatus.OK);
    }

    @PostMapping("/escolas/{escolaId}/endereco")
    @ResponseBody
    public ResponseEntity<String> salvarEnderecoEscola(
    		@Valid @RequestBody Endereco endereco, BindingResult result,
    		@PathVariable("escolaId") String id) {
    	
    	 if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	 }
    	
    	enderecoService.salvarEnderecoByEscola(id,endereco);
    	
    	return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
    @GetMapping("/responsaveis/{responsavelId}/endereco")
    @ResponseBody
    public ResponseEntity<Endereco> enderecoResponsavel(
    		@PathVariable("responsavelId") String id) {
    	
        return new ResponseEntity<Endereco>(enderecoService.enderecoByResponsavel(id), HttpStatus.OK);
    }
    
    @PostMapping("/responsaveis/{responsavelId}/endereco")
    @ResponseBody
    public ResponseEntity<String> salvarEnderecoResponsavel(
    		@Valid @RequestBody Endereco endereco, BindingResult result,
    		@PathVariable("responsavelId") String id) {
    	
    	if (result.hasErrors()) {
    		handleErrorBadRequestException(result);
    	}
    	
    	enderecoService.salvarEnderecoByResponsavel(id,endereco);
    	
    	return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}