package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Responsavel;
import com.tioapp.kombi.service.ResponsavelService;

@RestController
@RequestMapping(value = "/kombi/responsaveis", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResponsavelController {
	
	@Autowired
	private ResponsavelService responsavelService;
	
    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Responsavel> responsavel(
    		@PathVariable("id") String id) {
    	
        return new ResponseEntity<Responsavel>(responsavelService.responsavel(id), HttpStatus.OK);
    }
	
    @PostMapping("")
    @ResponseBody
    public ResponseEntity<String> salvarResponsavel(
    		@Valid @RequestBody Responsavel responsavel, BindingResult result) {
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
    	responsavelService.salvarResponsavel(responsavel);
    	
    	return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<String> atualizarResponsavel(
    		@PathVariable("id") String id,
    		@Valid @RequestBody Responsavel responsavel, BindingResult result) {
    	
    	responsavel.setId(id);
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
    	responsavelService.atualizarResponsavel(responsavel);
    	
    	return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }
    
	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}