package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Telefone;
import com.tioapp.kombi.service.TelefoneService;

@RestController
@RequestMapping(value = "/kombi", produces = MediaType.APPLICATION_JSON_VALUE)
public class TelefoneController {
	
	@Autowired
	private TelefoneService telefoneService;
	
	@GetMapping("/escolas/{telefoneId}/telefones")
    @ResponseBody
    public ResponseEntity<List<Telefone>> telefonesEscola(
    		@PathVariable("telefoneId") String id) {
    	
        return new ResponseEntity<List<Telefone>>(telefoneService.telefonesByEscola(id), HttpStatus.OK);
    }
    
	@PostMapping("/escolas/{telefoneId}/telefones")
    @ResponseBody
    public ResponseEntity<String> salvarTelefonesEscola(
    		@Valid @RequestBody List<Telefone> telefones, BindingResult result,
    		@PathVariable("telefoneId") String id) {
    	
	   	 if (result.hasErrors()) {
			 handleErrorBadRequestException(result);
		 }
		
		telefoneService.salvarTelefonesByEscola(id, telefones);
		
		return new ResponseEntity<String>(HttpStatus.CREATED);
	    
	}
	
	@GetMapping("/responsaveis/{responsavelId}/telefones")
    @ResponseBody
    public ResponseEntity<List<Telefone>> telefonesResponsavel(
    		@PathVariable("responsavelId") String id) {
    	
        return new ResponseEntity<List<Telefone>>(telefoneService.telefonesByResponsavel(id), HttpStatus.OK);
    }

	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}
