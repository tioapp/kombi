package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.service.EscolaService;

@RestController
@RequestMapping(value = "/kombi/escolas", produces = MediaType.APPLICATION_JSON_VALUE)
public class EscolaController {
	
	@Autowired
	private EscolaService escolaService;
	
    @GetMapping()
    @ResponseBody
    public ResponseEntity<List<Escola>> escolas(
    		@RequestParam(value = "nome", required = false) String nome,
    		@RequestParam(value = "regiao", required = false) String regiao) {
    	
        return new ResponseEntity<List<Escola>>(escolaService.escolas(nome, regiao), HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Escola> escola(
    		@PathVariable("id") String id) {
    	
        return new ResponseEntity<Escola>(escolaService.escola(id), HttpStatus.OK);
    }
    
	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}