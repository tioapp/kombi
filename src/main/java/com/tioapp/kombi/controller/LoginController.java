package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Login;
import com.tioapp.kombi.service.LoginService;

@RestController
@RequestMapping(value = "/kombi", produces = MediaType.APPLICATION_JSON_VALUE)
public class LoginController {

	@Autowired
	private LoginService loginService;
	
    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<Login> login(
    		@Valid @RequestBody Login login, BindingResult result) {
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
        return new ResponseEntity<Login>(loginService.login(login), HttpStatus.CREATED);
    }
	
    @PostMapping("/autenticacao")
    @ResponseBody
    public ResponseEntity<Login> autenticacao(
    		@Valid @RequestBody Login login, BindingResult result) {
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
        return new ResponseEntity<Login>(loginService.autenticacao(login), HttpStatus.OK);
    }
    
	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}