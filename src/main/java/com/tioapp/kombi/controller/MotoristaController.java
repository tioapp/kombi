package com.tioapp.kombi.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.model.Motorista;
import com.tioapp.kombi.service.MotoristaService;

@RestController
@RequestMapping(value = "/kombi/motoristas", produces = MediaType.APPLICATION_JSON_VALUE)
public class MotoristaController {
	
	@Autowired
	private MotoristaService motoristaService;
	
    @GetMapping()
    @ResponseBody
    public ResponseEntity<List<Motorista>> motoristas(
    		@RequestParam(value = "escolaId", required = false) String escolaId) {
    	
        return new ResponseEntity<List<Motorista>>(motoristaService.motoristas(escolaId), HttpStatus.OK);
    }
    
	
    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Motorista> motorista(
    		@PathVariable("id") String id) {
    	
        return new ResponseEntity<Motorista>(motoristaService.motorista(id), HttpStatus.OK);
    }
	
    @PostMapping("")
    @ResponseBody
    public ResponseEntity<String> salvarmotorista(
    		@Valid @RequestBody Motorista motorista, BindingResult result) {
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
    	motoristaService.salvarMotorista(motorista);
    	
    	return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<String> atualizarMotorista(
    		@PathVariable("id") String id,
    		@Valid @RequestBody Motorista motorista, BindingResult result) {
    	
    	motorista.setId(id);
    	
    	if (result.hasErrors()) {
    		 handleErrorBadRequestException(result);
    	}
    	
    	motoristaService.atualizarMotorista(motorista);
    	
    	return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }
    
    @PostMapping("/{id}/escolas")
    @ResponseBody
    public ResponseEntity<String> salvarEnderecoResponsavel(
    		@Valid @RequestBody Escola escola, BindingResult result,
    		@PathVariable("id") String id) {
    	
    	if (result.hasErrors()) {
    		handleErrorBadRequestException(result);
    	}
    	
    	motoristaService.salvarEscola(id,escola);
    	
    	return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
	public void handleErrorBadRequestException(BindingResult bindingResult) throws BadRequestException {
		List<FieldError> validationErrors = new ArrayList<FieldError>();
		if (bindingResult.hasErrors()){

	        for (org.springframework.validation.FieldError error :bindingResult.getFieldErrors()){
	        	FieldError build = FieldError.builder().error(error.getDefaultMessage()).name(error.getField()).build();
	        	validationErrors.add(build);
	        }

            throw new BadRequestException(validationErrors);
        }
	}
}