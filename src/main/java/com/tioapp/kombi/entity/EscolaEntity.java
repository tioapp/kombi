package com.tioapp.kombi.entity;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@Document(collection = "escola")
@ToString
public class EscolaEntity implements Serializable {
	
	private static final long serialVersionUID = 6736172368387667438L;
	
	@Id
	private String id;
	private String nome;
	private List<TelefoneEntity> telefones;
	private EnderecoEntity endereco;
	
//	public EscolaEntity(String id, String nome) {
//		this.id=id;
//		this.nome=nome;
//	}
	
}