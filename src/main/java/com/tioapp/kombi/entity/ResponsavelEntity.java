package com.tioapp.kombi.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@Document(collection = "responsavel")
@ToString
public class ResponsavelEntity implements Serializable {
	
	private static final long serialVersionUID = 1484212670341513641L;
	
	private String id;
	private String nome;
	
	@Indexed(unique=true)
	private String documentoCpf;
	
	private String documentoRg;
	private String estadoCivil;
	private LocalDate dataNascimento;
	
	@Indexed(unique=true)
	private String email;
	
	private List<TelefoneEntity> telefones;
	private EnderecoEntity endereco;
}