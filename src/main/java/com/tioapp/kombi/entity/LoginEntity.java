package com.tioapp.kombi.entity;

import java.io.Serializable;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@Document(collection = "login")
@CompoundIndexes({
    @CompoundIndex(def = "{'login' : 1, 'tipo': 1}", unique = true)
})
@ToString
public class LoginEntity implements Serializable {
	
	private static final long serialVersionUID = -7051598155926870784L;
	
	private String id;
	private String nome;
	private String login;
	private String senha;
	private String tipo;

}
