package com.tioapp.kombi.entity;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class TelefoneEntity implements Serializable{

	private static final long serialVersionUID = -5794991500290431125L;
	
	private String tipo;
	private String ddd;
	private String numero;
	
}