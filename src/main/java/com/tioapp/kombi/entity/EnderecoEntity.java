package com.tioapp.kombi.entity;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class EnderecoEntity implements Serializable{

	private static final long serialVersionUID = -535594303494814069L;
	
	private String rua;
	private String bairro;
	private String numero;
	private String complemento;
	private String cidade;
	private String estado;

}