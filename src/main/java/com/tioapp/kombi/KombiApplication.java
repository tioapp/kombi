package com.tioapp.kombi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KombiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KombiApplication.class, args);
	}

}
