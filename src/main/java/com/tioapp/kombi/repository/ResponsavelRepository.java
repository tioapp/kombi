package com.tioapp.kombi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tioapp.kombi.entity.ResponsavelEntity;

@Repository
public interface ResponsavelRepository extends MongoRepository<ResponsavelEntity, String> {}