package com.tioapp.kombi.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tioapp.kombi.entity.EscolaEntity;

@Repository
public interface EscolaRepository extends MongoRepository<EscolaEntity, String> {
	
	public List<EscolaEntity> findByNomeLike(String nome);
	public List<EscolaEntity> findByEnderecoBairroLike(String bairro);

}