package com.tioapp.kombi.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.MotoristaEntity;

@Repository
public interface MotoristaRepository extends MongoRepository<MotoristaEntity, String> {
	
	public List<MotoristaEntity> findByEscolasId(String id);
}