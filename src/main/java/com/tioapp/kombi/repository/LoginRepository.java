package com.tioapp.kombi.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tioapp.kombi.entity.LoginEntity;

public interface LoginRepository extends MongoRepository<LoginEntity, String> {
	
	public List<LoginEntity> findByLogin(String login);

}