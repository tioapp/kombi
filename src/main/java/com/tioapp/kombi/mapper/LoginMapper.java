package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.MotoristaController;
import com.tioapp.kombi.controller.ResponsavelController;
import com.tioapp.kombi.entity.LoginEntity;
import com.tioapp.kombi.model.Login;
import com.tioapp.kombi.model.Motorista;
import com.tioapp.kombi.model.Responsavel;

@Component
public final class LoginMapper {
	
	private LoginMapper() {}
	
	public Login buildLogin(LoginEntity loginEntity) {
		//Link linkResponsable = linkTo(methodOn(ResponsavelController.class).responsavel(responsavel.getId())).withRel("responsavel");

		return Login.builder()
				.id(loginEntity.getId())
				.nome(loginEntity.getNome())
				.login(loginEntity.getLogin())
				.build();
				//.add(linkResponsable);
	}
	
	public Login buildLogin(LoginEntity loginEntity, Responsavel responsavel) {
		Link linkResponsable = linkTo(methodOn(ResponsavelController.class).responsavel(responsavel.getId())).withRel("responsavel");

		return Login.builder()
				.id(loginEntity.getId())
				.nome(loginEntity.getNome())
				.login(loginEntity.getLogin())
				.build()
				.add(linkResponsable);
	}	
	
	public Login buildLogin(LoginEntity loginEntity, Motorista motorista) {
		Link linkResponsable = linkTo(methodOn(MotoristaController.class).motorista(motorista.getId())).withRel("motorista");

		return Login.builder()
				.id(loginEntity.getId())
				.nome(loginEntity.getNome())
				.login(loginEntity.getLogin())
				.build()
				.add(linkResponsable);
	}	
	
	public LoginEntity buildLoginEntity(Login login) {
		return LoginEntity.builder()
				.nome(StringUtils.upperCase(login.getNome()))
				.login(login.getLogin())
				.senha(login.getSenha())
				.tipo(login.getTipo().name())
				.build();
	}	
}