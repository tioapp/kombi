package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.MotoristaController;
import com.tioapp.kombi.entity.EnderecoEntity;
import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.MotoristaEntity;
import com.tioapp.kombi.entity.TelefoneEntity;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.model.Endereco;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.model.EstadoCivilEnum;
import com.tioapp.kombi.model.Login;
import com.tioapp.kombi.model.Motorista;

@Component
public final class MotoristaMapper {
	
	@Autowired
	private EnderecoMapper enderecoMapper;
	
	@Autowired
	private EscolaMapper escolaMapper;
	
	@Autowired
	private TelefoneMapper telefoneMapper;
	
	private MotoristaMapper() {}
	
	public List<Motorista> buildMotoristas(List<MotoristaEntity> motoristasEntity){
		List<Motorista> motoristas = new ArrayList<Motorista>();
		
		motoristasEntity.forEach(motoristaEntity -> {
			motoristas.add(this.buildMotorista(motoristaEntity));
		});
		
		if(!motoristas.isEmpty()) {
			return motoristas;
		
		} else {
			throw new NotFoundException();
		}
	
	}
	
	public Motorista buildMotorista(MotoristaEntity motoristaEntity) {
		Link linkSelf = linkTo(methodOn(MotoristaController.class).motorista(motoristaEntity.getId())).withSelfRel();
		//Link linkEscolas= linkTo(methodOn(EscolaController.class).escolas("", "")).withRel("escolas");
		
		return Motorista.builder()
				.id(motoristaEntity.getId())
				.nome(motoristaEntity.getNome())
				.documentoCpf(motoristaEntity.getDocumentoCpf())
				.documentoRg(motoristaEntity.getDocumentoRg())
				.estadoCivil(StringUtils.isNotBlank(motoristaEntity.getEstadoCivil()) ? EstadoCivilEnum.valueOf(motoristaEntity.getEstadoCivil())  : null)
				.dataNascimento(motoristaEntity.getDataNascimento())
				.email(motoristaEntity.getEmail())
				.endereco(enderecoMapper.buildEndereco(motoristaEntity.getId(), motoristaEntity.getEndereco()))
				.telefones(telefoneMapper.buildTelefones(motoristaEntity))
				.escolas(escolaMapper.buildEscolas(motoristaEntity.getEscolas()))
				.build()
				.add(linkSelf);
				//.add(linkEscolas);
	}
	
	public MotoristaEntity buildMotoristaEntity(Motorista motorista) {
		return MotoristaEntity.builder()
				.id(motorista.getId())
				.nome(motorista.getNome())
				.documentoCpf(motorista.getDocumentoCpf())
				.documentoRg(motorista.getDocumentoRg())
				.estadoCivil(motorista.getEstadoCivil() != null ? motorista.getEstadoCivil().name() : null)
				.dataNascimento(motorista.getDataNascimento())
				.email(motorista.getEmail())
				.build();

	}
	
	public MotoristaEntity buildMotoristaEntity(String id, String email, String nome) {
		return MotoristaEntity.builder()
				.id(id)
				.email(email)
				.nome(nome)
				.build();

	}
	
	public MotoristaEntity buildMotoristaEntity(Motorista motorista,EnderecoEntity enderecoEntity,List<TelefoneEntity> telefonesEntity) {
		return MotoristaEntity.builder()
				.id(motorista.getId())
				.nome(motorista.getNome())
				.documentoCpf(motorista.getDocumentoCpf())
				.documentoRg(motorista.getDocumentoRg())
				.estadoCivil(motorista.getEstadoCivil() != null ? motorista.getEstadoCivil().name() : null)
				.dataNascimento(motorista.getDataNascimento())
				.email(motorista.getEmail())
				.endereco(enderecoEntity)
				.telefones(telefonesEntity)
				.build();

	}
	
	public MotoristaEntity buildMotoristaEntity(Login login) {
		return MotoristaEntity.builder()
				.id(login.getId())
				.nome(login.getNome())
				.email(login.getLogin())
				.build();

	}
	
	public MotoristaEntity buildMotoristaEntity(MotoristaEntity motoristaEntity, Endereco endereco) {
		motoristaEntity.setEndereco(enderecoMapper.buildEnderecoEntity(endereco));
		
		return motoristaEntity;
	}
	
	public MotoristaEntity buildMotoristaEntity(MotoristaEntity motoristaEntity, Escola escola) {
		List<EscolaEntity> escolas = motoristaEntity.getEscolas();
		
		if (escolas == null) {
			escolas = new ArrayList<EscolaEntity>();
		}
			
		escolas.add(escolaMapper.buildEscolaEntity(escola));
		
		motoristaEntity.setEscolas(escolas);
		
		return motoristaEntity;
	}
}