package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.EnderecoController;
import com.tioapp.kombi.controller.EscolaController;
import com.tioapp.kombi.controller.TelefoneController;
import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.TelefoneEntity;
import com.tioapp.kombi.exception.BadRequestException;
import com.tioapp.kombi.exception.FieldError;
import com.tioapp.kombi.model.Endereco;
import com.tioapp.kombi.model.Escola;
import com.tioapp.kombi.model.Telefone;

@Component
public final class EscolaMapper {
	
	@Autowired
	private EnderecoMapper enderecoMapper;
	
	@Autowired
	private TelefoneMapper telefoneMapper;
	
	private EscolaMapper() {};
	
	public List<Escola> buildEscolas(List<EscolaEntity> escolaEntities) {
		List<Escola> escolas = new ArrayList<Escola>();
		
		escolaEntities.forEach(escolaEntity ->{
			escolas.add(buildEscola(escolaEntity));
		});
		
		return escolas;
	}
	
	public Escola buildEscola(EscolaEntity escolaEntity) {
		Link linkSelf = linkTo(methodOn(EscolaController.class).escola(escolaEntity.getId())).withSelfRel();
		Link linkTelefones = linkTo(methodOn(TelefoneController.class).telefonesEscola(escolaEntity.getId())).withRel("telefones");
		Link linkEndereco = linkTo(methodOn(EnderecoController.class).enderecoEscola(escolaEntity.getId())).withRel("endereco");
		
		return Escola.builder()
                .id(escolaEntity.getId())
                .nome(escolaEntity.getNome())
                .telefones(telefoneMapper.buildTelefones(escolaEntity))
                .endereco(enderecoMapper.buildEndereco(escolaEntity.getId(),escolaEntity.getEndereco()))
                .build()
                .add(linkSelf)
                .add(linkTelefones)
                .add(linkEndereco);
     
	}
	
	public EscolaEntity buildEscolaEntity(Escola escola) {
		return EscolaEntity.builder()
                .id(escola.getId())
                .nome(escola.getNome())
                .build();
	}
	
	public EscolaEntity buildEscolaEntity(EscolaEntity escolaEntity, Endereco endereco) {
		escolaEntity.setEndereco(enderecoMapper.buildEnderecoEntity(endereco));
		
		return escolaEntity;
	}
	
	public EscolaEntity buildTelefonesEntity(EscolaEntity escolaEntity, List<Telefone> telefones) {
		List<TelefoneEntity> telefonesEntity = telefoneMapper.buildTelefonesEntity(telefones);
		
		if (!telefonesEntity.isEmpty()) {
			escolaEntity.setTelefones(telefonesEntity);
			return escolaEntity;
		
		} else {
			List<FieldError> validationErrors = new ArrayList<FieldError>();
			validationErrors.add(FieldError.builder()
					.name("telefones")
					.error("Lista de Telefone invalida")
					.build());
			throw new BadRequestException(validationErrors);
		}
	}
}