package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.ResponsavelController;
import com.tioapp.kombi.entity.EnderecoEntity;
import com.tioapp.kombi.entity.ResponsavelEntity;
import com.tioapp.kombi.entity.TelefoneEntity;
import com.tioapp.kombi.model.Endereco;
import com.tioapp.kombi.model.EstadoCivilEnum;
import com.tioapp.kombi.model.Login;
import com.tioapp.kombi.model.Responsavel;

@Component
public final class ResponsavelMapper {
	
	@Autowired
	private EnderecoMapper enderecoMapper;
	
	private ResponsavelMapper() {}
	
	public Responsavel buildResponsavel(ResponsavelEntity responsavelEntity) {
		Link linkSelf = linkTo(methodOn(ResponsavelController.class).responsavel(responsavelEntity.getId())).withSelfRel();
		
		return Responsavel.builder()
				.id(responsavelEntity.getId())
				.nome(responsavelEntity.getNome())
				.documentoCpf(responsavelEntity.getDocumentoCpf())
				.documentoRg(responsavelEntity.getDocumentoRg())
				.estadoCivil(StringUtils.isNotBlank(responsavelEntity.getEstadoCivil()) ? EstadoCivilEnum.valueOf(responsavelEntity.getEstadoCivil())  : null)
				.dataNascimento(responsavelEntity.getDataNascimento())
				.email(responsavelEntity.getEmail())
				.build()
				.add(linkSelf);
	}
	
	
	public ResponsavelEntity buildResponsavelEntity(Responsavel responsavel) {
		return ResponsavelEntity.builder()
				.id(responsavel.getId())
				.nome(responsavel.getNome())
				.documentoCpf(responsavel.getDocumentoCpf())
				.documentoRg(responsavel.getDocumentoRg())
				.estadoCivil(responsavel.getEstadoCivil() != null ? responsavel.getEstadoCivil().name() : null)
				.dataNascimento(responsavel.getDataNascimento())
				.email(responsavel.getEmail())
				.build();

	}
	
	public ResponsavelEntity buildResponsavelEntity(Responsavel responsavel, EnderecoEntity enderecoEntity,List<TelefoneEntity> telefonesEntity) {
		return ResponsavelEntity.builder()
				.id(responsavel.getId())
				.nome(responsavel.getNome())
				.documentoCpf(responsavel.getDocumentoCpf())
				.documentoRg(responsavel.getDocumentoRg())
				.estadoCivil(responsavel.getEstadoCivil() != null ? responsavel.getEstadoCivil().name() : null)
				.dataNascimento(responsavel.getDataNascimento())
				.email(responsavel.getEmail())
				.endereco(enderecoEntity)
				.telefones(telefonesEntity)
				.build();

	}
	
	public ResponsavelEntity buildResponsavelEntity(String id, String email, String nome) {
		return ResponsavelEntity.builder()
				.id(id)
				.email(email)
				.nome(nome)
				.build();

	}
	
	public ResponsavelEntity buildResponsavelEntity(Login login) {
		return ResponsavelEntity.builder()
				.id(login.getId())
				.nome(login.getNome())
				.email(login.getLogin())
				.build();

	}
	
	public ResponsavelEntity buildResponsavelEntity(ResponsavelEntity responsavelEntity, Endereco endereco) {
		responsavelEntity.setEndereco(enderecoMapper.buildEnderecoEntity(endereco));
		
		return responsavelEntity;
	}
}