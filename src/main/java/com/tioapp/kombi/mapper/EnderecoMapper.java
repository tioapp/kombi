package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.EscolaController;
import com.tioapp.kombi.entity.EnderecoEntity;
import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.model.Endereco;

@Component
public final class EnderecoMapper {
	
	private EnderecoMapper() {}
	
	public Endereco buildEndereco(EscolaEntity escolaEntity){
		Link linkEscola = linkTo(methodOn(EscolaController.class).escola(escolaEntity.getId())).withRel("escolas");
		EnderecoEntity enderecoEntity = escolaEntity.getEndereco();
		
		if (enderecoEntity != null) {
			return Endereco.builder()
	                .rua(enderecoEntity.getRua())
	                .bairro(enderecoEntity.getBairro())
	                .numero(enderecoEntity.getNumero())
	                .complemento(enderecoEntity.getComplemento())
	                .cidade(enderecoEntity.getCidade())
	                .estado(enderecoEntity.getEstado())
	                .build()
	                .add(linkEscola);
		
		} else {
			throw new NotFoundException();
		}
	}
	
	public Endereco buildEndereco(String id, EnderecoEntity enderecoEntity){
		//Link linkEscola = linkTo(methodOn(EscolaController.class).escola(id)).withRel("escolas");
		
		if (enderecoEntity != null) {
			return Endereco.builder()
	                .rua(enderecoEntity.getRua())
	                .bairro(enderecoEntity.getBairro())
	                .numero(enderecoEntity.getNumero())
	                .complemento(enderecoEntity.getComplemento())
	                .cidade(enderecoEntity.getCidade())
	                .estado(enderecoEntity.getEstado())
	                .build();
	                //.add(linkEscola);
		
		}
		
		return null;
	}
	
	
	public EnderecoEntity buildEnderecoEntity(Endereco endereco) {
		return EnderecoEntity.builder()
                .rua(StringUtils.upperCase(endereco.getRua()))
                .bairro(StringUtils.upperCase(endereco.getBairro()))
                .numero(StringUtils.upperCase(endereco.getNumero()))
                .complemento(StringUtils.upperCase(endereco.getComplemento()))
                .cidade(StringUtils.upperCase(endereco.getCidade()))
                .estado(StringUtils.upperCase(endereco.getEstado()))
                .build();
	}
}