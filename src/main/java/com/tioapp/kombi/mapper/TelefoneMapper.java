package com.tioapp.kombi.mapper;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.tioapp.kombi.controller.EscolaController;
import com.tioapp.kombi.entity.EscolaEntity;
import com.tioapp.kombi.entity.MotoristaEntity;
import com.tioapp.kombi.entity.ResponsavelEntity;
import com.tioapp.kombi.entity.TelefoneEntity;
import com.tioapp.kombi.exception.NotFoundException;
import com.tioapp.kombi.model.Telefone;
import com.tioapp.kombi.model.TipoTelefoneEnum;

@Component
public final class TelefoneMapper {
	
	private TelefoneMapper() {}
	
	public List<Telefone> buildTelefones(EscolaEntity escolaEntity){
		List<Telefone> telefones = new ArrayList<Telefone>();
		
		if (escolaEntity.getTelefones() != null ) {
			escolaEntity.getTelefones().forEach(telefoneEntity ->{
				if (telefoneEntity != null) {
					telefones.add(this.buildTelefone(telefoneEntity, escolaEntity.getId()));
				}
			});
		}

		return telefones;
			
	}
	
	public List<Telefone> buildTelefones(ResponsavelEntity responsavelEntity){
		List<Telefone> telefones = new ArrayList<Telefone>();
		
		if (responsavelEntity.getTelefones() != null ) {
			responsavelEntity.getTelefones().forEach(telefoneEntity ->{
				if (telefoneEntity != null) {
					telefones.add(this.buildTelefone(telefoneEntity, responsavelEntity.getId()));
				}
			});
		}

		return telefones;
			
	}
	
	public List<Telefone> buildTelefones(MotoristaEntity escolaEntity){
		List<Telefone> telefones = new ArrayList<Telefone>();
		
		if (escolaEntity.getTelefones() != null ) {
			escolaEntity.getTelefones().forEach(telefoneEntity ->{
				if (telefoneEntity != null) {
					telefones.add(this.buildTelefone(telefoneEntity, escolaEntity.getId()));
				}
			});
		}

		return telefones;
			
	}
	
	public Telefone buildTelefone(TelefoneEntity telefoneEntity, String escolaId) {
		Link linkEscola = linkTo(methodOn(EscolaController.class).escola(escolaId)).withRel("escolas");
		
		return Telefone.builder()
                .tipo(TipoTelefoneEnum.valueOf(telefoneEntity.getTipo()))
                .ddd(telefoneEntity.getDdd())
                .numero(telefoneEntity.getNumero())
                .build()
                .add(linkEscola);
	}
	
	public List<TelefoneEntity> buildTelefonesEntity(List<Telefone> telefones) {
		List<TelefoneEntity> telefonesEntity = new ArrayList<TelefoneEntity>();
		
		telefones.forEach(telefone  -> {
			if(StringUtils.isNotBlank(
					telefone.getDdd()) && 
					StringUtils.isNotBlank(telefone.getNumero()) && 
					telefone.getTipo() != null) {
				
						telefonesEntity.add(TelefoneEntity.builder()
								.tipo(telefone.getTipo().name())
								.ddd(telefone.getDdd())
								.numero(telefone.getNumero())
								.build());
			}
		});
		
		return telefonesEntity;
		
	}
}